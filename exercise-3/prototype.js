
String.prototype.ucfirst = function() {
    return this[0].toUpperCase() + this.substring(1);
}


String.prototype.vig = function (code) {

    if(typeof code !== "string" || !code) return '';

    while(code.length < this.length) {
        code += code;
    }

    let codeIndex = 0;
    return this.toLowerCase().split('').map(function(char) {

        // Position dans l'alphabet du car du message
        const charCode = char.charCodeAt(0) - "a".charCodeAt(0);

        //Vérifie si car is alphabet
        if (charCode < 0 || charCode > 25) return char;

        //Position dans l'alphabet du car du code
        const codeCode = code[codeIndex++].charCodeAt(0) - "a".charCodeAt(0);

        //On applique vigenere
        const cryptedCode = (charCode + codeCode) % 26;

        //Position du car codé dans la table ASCII
        const cryptedChar = cryptedCode + "a".charCodeAt(0);

        //On récupere le code
        return String.fromCharCode(cryptedChar);
    }).join("");
}

Object.prototype.prop_access = function (str) {
    if(typeof str !== "string" || !str) return "";

    var object = this;
    var props = str.split(".");

    for (let i = 0; i < props.length; i++){

        if(object.hasOwnProperty(props[i])){
            object = object[props[i]];
        }else {
            return "Propriété indéfini";
        }
    }
    return object;
}

console.log({animal : { type: {name : "issa", age: 23}}}.prop_access("animal.type.name"));
