function snake_case(str) {
    if(typeof str != "string" || !str) return "";

    return str.toLowerCase().replace(/ /g,"_");
}

function leet(str) {
    if(typeof str != "string" || !str) return "";
    str = str.toLowerCase();
    let code = { "a":4, "e":3, "i":1, "o":0, "u":"(_)", "y":7};

    for (let i in code){
        let regex = new  RegExp(i, "g");
        str = str.replace(regex, code[i]);
    }
    return str;
}
