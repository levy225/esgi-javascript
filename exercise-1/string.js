function ucfirst(str){
    if(typeof str != "string" || !str) return "";

    return str[0].toUpperCase() + str.substring(1);
}

function capitalize(str) {
    if(typeof str != "string" || !str) return "";
    return str.toLowerCase().split(" ").map(function (item){
        return ucfirst(item);
    }).join(" ")
}

function camelCase(str) {
    if(typeof str != "string" || !str) return "";
    return capitalize(str).replace(/ /g,"");
}

function snake_case(str) {
    if(typeof str != "string" || !str) return "";
    return str.toLowerCase().replace(/[^a-zA-Z0-9]/,"_");

}

function leet(str) {
    if(typeof str != "string" || !str) return "";
    return str.replace(/[aeiouy]/gi, function(item){
        switch(item){
            case 'a':
            case 'A':
                return 4;
            case 'e':
            case 'E':
                return 3;
            case 'i':
            case 'I':
                return 1;
            case 'o':
            case 'O':
                return 0;
            case 'u':
            case 'U':
                return '(_)';
            case 'y':
            case 'Y':
                return 7;
        }
    });
}


function verlan(str) {
    if(typeof str !== "string" || !str) return "";
    return str.split(" ").map(function(word){
        return word.split("").reverse().join("");
    }).join(" ");
}

function yoda(str) {
    if(typeof str !== "string" || !str) return "";
    return str.split(' ').reverse().join(' ')
}

function vig(str, code) {
    if(typeof str !== "string" || !str) return '';
    if(typeof code !== "string" || !code) return '';

    while(code.length < str.length) {
        code += code;
    }

    let codeIndex = 0;
    return str.toLowerCase().split('').map(function(char) {

        // Position dans l'alphabet du car du message
        const charCode = char.charCodeAt(0) - "a".charCodeAt(0);

        //Vérifie si car is alphabet
        if (charCode < 0 || charCode > 25) return char;

        //Position dans l'alphabet du car du code
        const codeCode = code[codeIndex++].charCodeAt(0) - "a".charCodeAt(0);

        //On applique vigenere
        const cryptedCode = (charCode + codeCode) % 26;

        //Position du car codé dans la table ASCII
        const cryptedChar = cryptedCode + "a".charCodeAt(0);

        //On récupere le code
        return String.fromCharCode(cryptedChar);
    }).join("");;

}

function prop_access(object, str) {
    if(typeof str !== "string" || !str) return "";
    if(typeof object !== "object" || !object) return "";


    var props = str.split(".");
    for (let i = 0; i < props.length; i++){

        if(object.hasOwnProperty(props[i])){
            object = object[props[i]];
        }else {
            return "Propriété indéfini";
        }
    }

    return object;
}

console.log(prop_access({animal : { type: {name : "issa", age: 23}}}, "animal.type.name"));